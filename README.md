# Notes

### Settings


```
// config.json

{

    "db_config": {
        "db_url": "", // MongoDB URL
        "db_name": "notes", // MongoDB name
        "cl_name": "created_notes" // MongoDB collection name
    },

    "config": {
        "port": 3113, // Application port
        "passphrase_length": 16 // Random passphrase length
    }

}
```

### Dependencies

* Node.js
* MongoDB

### Launch

1. git clone *rep.url*
2. npm install
3. npm run start (for standart mode) npm run dev (for dev mode)
4. ?!?!?!?!
5. **PROFIT!**