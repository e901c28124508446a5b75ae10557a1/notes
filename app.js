const express = require('express');
const bodyParser = require('body-parser');
const mongosee = require('mongoose');
const path = require('path');
const appConfig = require('./config.json');
const { version } = require('./package.json');

const app = express();

const notes = require('./routes/notes.route');

app.use('/', notes);

app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'pug');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongosee.connect(appConfig.db_config.db_url, { useNewUrlParser: true });
mongosee.Promise = global.Promise;

let db = mongosee.connection;

db.on('Error', console.error.bind(console, 'MongoDB connection error: '));

app.listen(appConfig.config.port, () => {
    console.log('Welcome to Notes, port ' + appConfig.config.port);
});

app.use(function(req, res, next) {
    res.status(404).render('404', {
        title: 'Notes - 404',
        version: version
    });
});