const aes256 = require('aes256');
const unique = require('unique-string');
const hex = require('hex.js');
const appConfig = require('../config.json');
const { version } = require('../package.json');

const Notes = require('../models/notes.model');

exports.view_index = function (req, res) {
    res.render('index', {
        title: 'Notes - Main',
        passphrase: createRandomPassphrase(),
        version: version
    });
}

exports.note_create = function (req, res) {
    let note = new Notes({
        hash: unique(),
        note: hex.hex(aes256.encrypt(req.body.passphrase, req.body.note)),
        status: 1
    });
    note.save(function (err) {
        if (err) {
            return console.log(err);
        }
        res.redirect('/create/' + note['hash'] + '/' + req.body.passphrase);
    });
}

exports.note_result = function (req, res) {
    res.render('result', {
        title: 'Notes - Result',
        protocol: req.protocol,
        host: req.hostname,
        port: appConfig.config.port,
        version: version,
        hash: req.params['hash'],
        passphrase: req.params['passphrase']
    });
}

exports.note_get = function (req, res) {
    var object = { hash: req.params.hash };
    Notes.findOne(object, function (err, note) {
        if (err) {
            res.redirect('/');
        }
        try {
            var queredData = note['note'];
            var encData = hex.unHex(queredData);
            var endData = aes256.decrypt(req.params.passphrase, encData);
            res.render('view', {
                title: 'Notes - View',
                encData: endData,
                version: version
            });
            deleteNote(req.params.hash);
        } catch (e) {
            res.redirect('/');
        }
    });
}

function createRandomPassphrase() {
    var chars = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'
    var passphrase = '';
    for (var x = 0; x < appConfig.config.passphrase_length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        passphrase += chars.charAt(i);
    }
    return passphrase;
}

function deleteNote(hash) {
    var object = { hash: hash };
    Notes.deleteOne(object, function (err) {
        if (err) {
            console.log(err);
        }
    });
}