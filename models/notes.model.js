const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NotesSchema = new Schema({
    hash: {type: String, required: true},
    note: {type: String, required: true},
});

module.exports = mongoose.model('Notes', NotesSchema);