const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

const urlencodedParser = bodyParser.urlencoded({ extended: false });

const notes_controller = require('../controllers/notes.controller');

router.post('/create', urlencodedParser, notes_controller.note_create);
router.get('/view/:hash/:passphrase', urlencodedParser, notes_controller.note_get);
router.get('/create/:hash/:passphrase', urlencodedParser, notes_controller.note_result);
router.get('/', urlencodedParser, notes_controller.view_index);

module.exports = router;